﻿using MySql.Data.MySqlClient;
using System;
using Dapper;
using System.Text;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string server="server=10.106.65.33;database=test;uid=root;pwd=QAZwsx123;charset='gbk'";
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            MySqlConnection con = new MySqlConnection(server);
            //新增数据
            con.Execute("insert into user values(null, 'zzhi', 'http://zzhi.wang/', 30)");
            //新增数据返回自增id
            var id = con.QueryFirst<int>("insert into user values(null, 'wang', 'http://www.cnblogs.com/linezero/', 18);select last_insert_id();");
            //修改数据
            con.Execute("update user set UserName = 'zhang zhi' where Id = @Id", new { Id = id });
            //查询数据
            var list = con.Query<User>("select * from user");
            foreach (var item in list)
            {
                Console.WriteLine($"用户名：{item.UserName} 链接：{item.Url}");
            }
            //删除数据
            con.Execute("delete from user where Id = @Id", new { Id = id });
            Console.WriteLine("删除数据后的结果");
            list = con.Query<User>("select * from user");
            foreach (var item in list)
            {
                Console.WriteLine($"用户名：{item.UserName} 链接：{item.Url}");
            }
            Console.ReadKey();
        }
    }
}
